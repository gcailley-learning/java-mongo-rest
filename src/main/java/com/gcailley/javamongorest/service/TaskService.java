package com.gcailley.javamongorest.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.gcailley.javamongorest.dto.TaskDTO;
import com.gcailley.javamongorest.model.Task;
import com.gcailley.javamongorest.repository.TaskRepository;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaskService {

    @Autowired
    private TaskRepository taskRepository;

    public List<Task> findAll() {
        Iterable<Task> tasks = this.taskRepository.findAll();
        return Lists.newArrayList(tasks);
    }

    public Task findById(String id) {
        Optional<Task> task = this.taskRepository.findById(id);
        return task.get();
    }

    public void deleteById(String id) {
        this.taskRepository.deleteById(id);
    }

    public Task save(Task task) {
        return this.taskRepository.save(task);
    }
}
