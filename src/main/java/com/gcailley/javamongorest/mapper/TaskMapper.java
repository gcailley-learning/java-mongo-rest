package com.gcailley.javamongorest.mapper;

import com.gcailley.javamongorest.dto.TaskDTO;
import com.gcailley.javamongorest.model.Task;

import fr.xebia.extras.selma.Mapper;


@Mapper()
public interface TaskMapper {
    // This will build a fresh new OrderDto
    TaskDTO asTaskDto(Task in);

    // This will update the given order
    Task asTask(TaskDTO in, Task out);
}
