package com.gcailley.javamongorest.repository;

import com.gcailley.javamongorest.model.Task;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface TaskRepository extends MongoRepository<Task, String> {
}
