package com.gcailley.javamongorest.controller;

import java.util.List;
import java.util.stream.Collectors;

import com.gcailley.javamongorest.dto.TaskDTO;
import com.gcailley.javamongorest.mapper.TaskMapper;
import com.gcailley.javamongorest.service.TaskService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import fr.xebia.extras.selma.Selma;
import lombok.extern.slf4j.Slf4j;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

@Slf4j
@RestController
public class TaskController {

	@Autowired
	private TaskService taskService;

	// Get SelmaMapper
	ModelMapper modelMapper = new ModelMapper();

	@GetMapping("/tasks")
	@ResponseStatus(HttpStatus.OK)
	public List<TaskDTO> getTasks(@RequestParam(value = "name", defaultValue = "World") String name) {
		log.info(String.format("TasksController:getTasks: %s", name));
		return taskService.findAll().stream()
			.map(task -> 
				modelMapper.map(task, TaskDTO.class)
			)
			.collect(Collectors.toList());
	}

	@GetMapping("/tasks/{id}")
	@ResponseStatus(HttpStatus.OK)
	public String getTask(@PathVariable("id") String taskId) {
		log.info(String.format("TasksController:getTask: %s", taskId));
		return String.format("Get %s!", taskId);
	}

	@PostMapping(value = "/tasks", consumes = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public TaskDTO createTask(@RequestBody TaskDTO taskDTO) {
		log.info(String.format("TasksController:getTask: %s", taskDTO.toString()));
		String.format("Post %s!", taskDTO.getName());

		return taskDTO;
	}
}
