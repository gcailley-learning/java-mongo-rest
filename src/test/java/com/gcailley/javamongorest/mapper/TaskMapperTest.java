package com.gcailley.javamongorest.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.gcailley.javamongorest.TaskUtil;
import com.gcailley.javamongorest.dto.TaskDTO;
import com.gcailley.javamongorest.model.Task;

import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;

import fr.xebia.extras.selma.Selma;

public class TaskMapperTest {
   	// Get SelmaMapper
	TaskMapper mapper = Selma.builder(TaskMapper.class).build();

    @Test
    public void toDto() {
        Task task = TaskUtil.getTask(1);

        ModelMapper modelMapper = new ModelMapper();
        TaskDTO taskDTO = modelMapper.map(task, TaskDTO.class);

        assertEquals(task.getUpdatedBy(), taskDTO.getUpdatedBy());
        assertEquals(task.getId(), taskDTO.getId());
        assertEquals(task.getName(), taskDTO.getName());
        assertEquals(task.getDescription(), taskDTO.getDescription());
        assertEquals(task.getCreatedBy(), taskDTO.getCreatedBy());
        assertEquals(task.getCreatedDate(), taskDTO.getCreatedDate());
        assertEquals(task.getUpdatedDate(), taskDTO.getUpdatedDate());
    }
    
}
