package com.gcailley.javamongorest.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.Optional;

import com.gcailley.javamongorest.TaskUtil;
import com.gcailley.javamongorest.model.Task;
import com.gcailley.javamongorest.repository.TaskRepository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class TaskServiceTest {

    @MockBean
    TaskRepository taskRepository;

    @Autowired
    TaskService taskService;

    @Test
    public void findAll() throws Exception {
        int numberElt = 5;
        Iterable<Task> expected = TaskUtil.getTasks(numberElt);
        doReturn(expected).when(this.taskRepository).findAll();

        List<Task> actuel = this.taskService.findAll();

        verify(this.taskRepository, times(1)).findAll();

        assertEquals(numberElt, actuel.size());
    }

    @Test
    public void findById() throws Exception {
        Task expected = TaskUtil.getTask(1);
        doReturn(Optional.of(expected)).when(this.taskRepository).findById(expected.getId());

        Task actual = this.taskService.findById(expected.getId());

        verify(this.taskRepository, times(1)).findById(expected.getId());

        assertEquals(expected, actual);
    }

    @Test
    public void deleteById() throws Exception {
        Task expected = TaskUtil.getTask(1);
        doNothing().when(this.taskRepository).deleteById(expected.getId());

        this.taskService.deleteById(expected.getId());

        verify(this.taskRepository, times(1)).deleteById(expected.getId());
    }

    @Test
    public void save() throws Exception {
        Task expected = TaskUtil.getTask(1);
        doReturn(expected).when(this.taskRepository).save(expected);

        Task actual = this.taskService.save(expected);

        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getDescription(), actual.getDescription());
    }

  
}
