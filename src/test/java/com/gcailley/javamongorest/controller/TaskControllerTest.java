package com.gcailley.javamongorest.controller;

import org.assertj.core.internal.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;
import org.springframework.test.web.servlet.MockMvc;

import lombok.extern.slf4j.Slf4j;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static org.mockito.Mockito.doReturn;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gcailley.javamongorest.TaskUtil;
import com.gcailley.javamongorest.dto.TaskDTO;
import com.gcailley.javamongorest.model.Task;
import com.gcailley.javamongorest.service.TaskService;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

@SpringJUnitWebConfig
@ContextConfiguration
@WebMvcTest(controllers = TaskController.class)
public class TaskControllerTest {
    @Autowired
    private MockMvc mockMvc;
    
    @MockBean
    private TaskService taskService;

    @Test
    public void getTasksList() throws Exception {
        
        // Setup our mocked service
        Task task1 = TaskUtil.getTask(1);
        Task task2 = TaskUtil.getTask(2);
        List<Task> tasks = new ArrayList<Task>();
        tasks.add(task1);
        tasks.add(task2);
        doReturn(tasks).when(taskService).findAll();

        mockMvc.perform(get("/tasks"))
        .andExpect(status().is(200))
        .andExpect(jsonPath("$", hasSize(2)))
        .andExpect(jsonPath("$[0].id", is(task1.getId())))
        .andExpect(jsonPath("$[0].name", is(task1.getName())))
        .andExpect(jsonPath("$[0].description", is(task1.getDescription())))
        .andExpect(jsonPath("$[0].createdDate", is(task1.getCreatedDate())))
        .andExpect(jsonPath("$[0].createdBy", is(task1.getCreatedBy())))
        .andExpect(jsonPath("$[0].updatedDate", is(task1.getUpdatedDate())))
        .andExpect(jsonPath("$[0].updatedBy", is(task1.getUpdatedBy())))
        .andExpect(jsonPath("$[1].id", is(task2.getId())));
    }

    @Test
    public void getATask() throws Exception {
        mockMvc.perform(get("/tasks/1")).andExpect(status().is(200));
        // .andExpect(jsonPath("$.id", is(1)));
    }

    @Test
    public void createATask() throws Exception {
        String message = asJsonString(TaskDTO.builder().name("great").build());
        mockMvc.perform(post("/tasks").contentType(MediaType.APPLICATION_JSON).content(message))
                .andExpect(status().is(201));
    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
