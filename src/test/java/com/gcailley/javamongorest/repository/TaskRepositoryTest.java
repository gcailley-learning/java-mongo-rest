package com.gcailley.javamongorest.repository;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.Optional;

import com.gcailley.javamongorest.TaskUtil;
import com.gcailley.javamongorest.model.Task;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TaskRepositoryTest {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    TaskRepository taskRepository;

    @Test
    public void deleteById_withoutMock() throws Exception {
        Task expected = TaskUtil.getTask(null);

        Task saved = this.taskRepository.save(expected);
        assertNotNull(saved);
        assertNotNull(saved.getId());

        Optional<Task> find = this.taskRepository.findById(saved.getId());
        assertNotNull(find.get());

        List<Task> tasksFind = this.taskRepository.findAll();
        assertEquals(1, tasksFind.size());

        this.taskRepository.deleteById(find.get().getId());
        tasksFind = this.taskRepository.findAll();
        assertEquals(1, tasksFind.size());

        verify(this.taskRepository, times(1)).save(expected);
        verify(this.taskRepository, times(1)).findById(any());
        verify(this.taskRepository, times(2)).findAll();
        verify(this.taskRepository, times(1)).deleteById(any());
    }
}
