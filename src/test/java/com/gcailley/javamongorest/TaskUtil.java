package com.gcailley.javamongorest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.gcailley.javamongorest.dto.TaskDTO;
import com.gcailley.javamongorest.model.Task;

public class TaskUtil {
    public static Task getTask(Integer id) {
        String now = new Date().toString();
        String idDb = id != null ? id.toString() : null;
        return new Task(idDb, "Name" + id, "Description " + id, now, "creator" + id, now,
                "updator" + id);
    }

    public static Iterable<Task> getTasks(Integer id) {
        List<Task> tasks = new ArrayList<Task>(id);
        for (int i = 0; i < id; i++) {
            tasks.add(getTask(i));
        }
        return tasks;
    }

    public static TaskDTO getTaskDTO(Integer id) {
        String now = new Date().toString();
        return new TaskDTO(id.toString(), "Name" + id, "Description " + id, now, "creator", now, "updator");
    }
}
